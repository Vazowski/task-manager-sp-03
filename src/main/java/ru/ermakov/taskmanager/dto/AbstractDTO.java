package ru.ermakov.taskmanager.dto;

import org.jetbrains.annotations.Nullable;

import java.util.UUID;

public class AbstractDTO {

    private String id = UUID.randomUUID().toString();

    @Nullable
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
