package ru.ermakov.taskmanager.controller;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.ermakov.taskmanager.api.service.IProjectService;
import ru.ermakov.taskmanager.api.service.ITaskService;
import ru.ermakov.taskmanager.enumerate.ReadinessStatus;
import ru.ermakov.taskmanager.dto.ProjectDTO;
import ru.ermakov.taskmanager.dto.TaskDTO;

import java.util.Collections;
import java.util.List;

@Controller
public class TaskController {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private ITaskService taskService;

    @Nullable
    private ModelAndView modelAndView;

    @Nullable
    private List<ProjectDTO> projectDTOList;

    @Nullable
    @RequestMapping(value = "/task/add", method = RequestMethod.GET)
    public ModelAndView taskAddPage() {
        modelAndView = new ModelAndView();
        projectDTOList = projectService.findAll();
        modelAndView.setViewName("task/edit");
        modelAndView.addObject("taskDTO", new TaskDTO());
        modelAndView.addObject("statusList", ReadinessStatus.values());
        modelAndView.addObject("projectDTOList", projectDTOList);
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/task/add", method = RequestMethod.POST)
    public ModelAndView taskAdd(@Nullable @ModelAttribute("task") TaskDTO taskDTO) {
        modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/tasks");
        taskService.update(taskDTO);
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/task/edit/{id}", method = RequestMethod.GET)
    public ModelAndView taskEdit(@Nullable @PathVariable final String id) {
        modelAndView = new ModelAndView();
        @Nullable final TaskDTO taskDTO = taskService.findById(id);
        projectDTOList = projectService.findAll();
        modelAndView.setViewName("task/edit");
        modelAndView.addObject("taskDTO", taskDTO);
        modelAndView.addObject("statusList", ReadinessStatus.values());
        modelAndView.addObject("projectDTOList", projectDTOList);
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/task/edit", method = RequestMethod.POST)
    public ModelAndView taskUpdate(@Nullable @ModelAttribute("taskDTO") TaskDTO taskDTO) {
        modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/tasks");
        taskService.update(taskDTO);
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/task/view/{id}", method = RequestMethod.GET)
    public ModelAndView taskView(@Nullable @PathVariable final String id) {
        @Nullable final TaskDTO taskDTO = taskService.findById(id);
        modelAndView = new ModelAndView();
        modelAndView.setViewName("task/view");
        modelAndView.addObject("taskDTO", taskDTO);
        modelAndView.addObject("statusList", ReadinessStatus.values());
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/tasks", method = RequestMethod.GET)
    public ModelAndView taskList() {
        @Nullable final List<TaskDTO> taskDTOList = taskService.findAll();
        modelAndView = new ModelAndView();
        projectDTOList = projectService.findAll();
        modelAndView.setViewName("task/list");
        modelAndView.addObject("projectDTOList", projectDTOList);
        modelAndView.addObject("taskDTOList", taskDTOList);
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/task/remove/{id}", method = RequestMethod.GET)
    public ModelAndView remove(@Nullable @PathVariable final String id) {
        taskService.remove(id);
        @Nullable final List<TaskDTO> taskDTOList = taskService.findAll();
        modelAndView = new ModelAndView();
        modelAndView.setViewName("task/list");
        modelAndView.addObject("taskDTOList", taskDTOList);
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/task/removeAll", method = RequestMethod.GET)
    public ModelAndView removeAll() {
        taskService.removeAll();
        @Nullable final List<TaskDTO> taskDTOList = Collections.emptyList();
        modelAndView = new ModelAndView();
        modelAndView.setViewName("task/list");
        modelAndView.addObject("taskDTOList", taskDTOList);
        return modelAndView;
    }
}
