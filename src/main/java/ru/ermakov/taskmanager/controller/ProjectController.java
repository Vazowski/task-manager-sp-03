package ru.ermakov.taskmanager.controller;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.ermakov.taskmanager.api.service.IProjectService;
import ru.ermakov.taskmanager.enumerate.ReadinessStatus;
import ru.ermakov.taskmanager.dto.ProjectDTO;

import java.util.Collections;
import java.util.List;

@Controller
public class ProjectController {

    @Autowired
    private IProjectService projectService;

    @Nullable
    private ModelAndView modelAndView;

    @Nullable
    @RequestMapping(value = "/project/add", method = RequestMethod.GET)
    public ModelAndView projectAddPage() {
        modelAndView = new ModelAndView();
        modelAndView.setViewName("project/edit");
        modelAndView.addObject("statusList", ReadinessStatus.values());
        modelAndView.addObject("projectDTO", new ProjectDTO());
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/project/add", method = RequestMethod.POST)
    public ModelAndView projectAdd(@Nullable @ModelAttribute("projectDTO") ProjectDTO projectDTO) {
        modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/projects");
        projectService.update(projectDTO);
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/project/edit/{id}", method = RequestMethod.GET)
    public ModelAndView projectEdit(@Nullable @PathVariable final String id) {
        modelAndView = new ModelAndView();
        modelAndView.setViewName("project/edit");
        modelAndView.addObject("projectDTO", projectService.findById(id));
        modelAndView.addObject("statusList", ReadinessStatus.values());
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/project/edit", method = RequestMethod.POST)
    public ModelAndView projectUpdate(@Nullable @ModelAttribute("projectDTO") ProjectDTO projectDTO) {
        modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/projects");
        projectService.update(projectDTO);
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/project/view/{id}", method = RequestMethod.GET)
    public ModelAndView projectView(@Nullable @PathVariable final String id) {
        @Nullable final ProjectDTO projectDTO = projectService.findById(id);
        modelAndView = new ModelAndView();
        modelAndView.setViewName("project/view");
        modelAndView.addObject("projectDTO", projectDTO);
        modelAndView.addObject("statusList", ReadinessStatus.values());
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/projects", method = RequestMethod.GET)
    public ModelAndView projectList() {
        @Nullable final List<ProjectDTO> projectDTOList = projectService.findAll();
        modelAndView = new ModelAndView();
        modelAndView.setViewName("project/list");
        modelAndView.addObject("projectDTOList", projectDTOList);
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/project/remove/{id}", method = RequestMethod.GET)
    public ModelAndView remove(@Nullable @PathVariable final String id) {
        projectService.remove(id);
        @Nullable final List<ProjectDTO> projectDTOList = projectService.findAll();
        modelAndView = new ModelAndView();
        modelAndView.setViewName("project/list");
        modelAndView.addObject("projectDTOList", projectDTOList);
        return modelAndView;
    }

    @Nullable
    @RequestMapping(value = "/project/removeAll", method = RequestMethod.GET)
    public ModelAndView removeAll() {
        projectService.removeAll();
        @Nullable final List<ProjectDTO> projectDTOList = Collections.emptyList();
        modelAndView = new ModelAndView();
        modelAndView.setViewName("project/list");
        modelAndView.addObject("projectDTOList", projectDTOList);
        return modelAndView;
    }
}
