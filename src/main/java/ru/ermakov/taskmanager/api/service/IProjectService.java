package ru.ermakov.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermakov.taskmanager.dto.ProjectDTO;

import java.util.List;

public interface IProjectService {

    void update(@Nullable final ProjectDTO projectDTO);

    ProjectDTO findById(@Nullable final String id);

    ProjectDTO findByName(@Nullable final String name);

    List<ProjectDTO> findAll();

    void remove(@NotNull final String id);

    void removeAll();
}
