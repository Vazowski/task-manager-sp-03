package ru.ermakov.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermakov.taskmanager.dto.TaskDTO;

import java.util.List;

public interface ITaskService {

    void update(@Nullable final TaskDTO taskDTO);

    TaskDTO findById(@Nullable final String id);

    TaskDTO findByName(@Nullable final String name);

    List<TaskDTO> findAll();

    void remove(@NotNull final String id);

    void removeAll();
}