package ru.ermakov.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ermakov.taskmanager.dto.AbstractDTO;

import java.util.List;

public interface IAbstractRepository<T extends AbstractDTO> {

    T findById(@NotNull final String id);

    List<T> findAll();

    void remove(@NotNull final String id);

    void clear();
}
