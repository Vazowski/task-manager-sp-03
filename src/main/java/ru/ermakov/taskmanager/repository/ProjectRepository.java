package ru.ermakov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.ermakov.taskmanager.model.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

    @Nullable
    @Query(value = "SELECT a FROM Project a WHERE a.name=:name")
    Project findByName(@Param ("name") @NotNull final String name);
}