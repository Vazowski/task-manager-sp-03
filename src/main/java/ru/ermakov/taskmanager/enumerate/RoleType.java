package ru.ermakov.taskmanager.enumerate;

import org.jetbrains.annotations.Nullable;

public enum RoleType {

    ADMIN("Administrator"),
    USER("User");

    @Nullable
    private String displayName;

    RoleType(final String displayName) {
        this.displayName = displayName;
    }

    @Nullable
    @Override
    public String toString() {
        return displayName;
    }
}
