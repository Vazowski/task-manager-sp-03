<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Task edit</title>
</head>
<body style="padding: 0px 30px">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <span class="navbar-brand mb-0 h1">Task manager</span>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <a class="nav-item nav-link" href="/projects">Project</a>
            <a class="nav-item nav-link" href="/tasks">Task</a>
            <a class="nav-item nav-link" href="#">User</a>
        </ul>
    </div>
</nav>
<c:if test="${empty taskDTO.id}">
    <c:url value="/task/add" var="var"/>
</c:if>
<c:if test="${!empty taskDTO.id}">
    <c:url value="/task/edit" var="var"/>
</c:if>
<form action="${var}" method="POST" style="padding-left: 50px">
    <c:if test="${!empty taskDTO.id}">
        <input type="hidden" name="id" value="${taskDTO.id}">
    </c:if>
    <div class="form-row">
        <div class="form-group col-md-2">
            <label for="inputName">Name</label>
            <input type="text" class="form-control" id="inputName" name="name" value="${taskDTO.name}">
        </div>
        <div class="form-group col-md-2">
            <label for="inputDateBegin">DateBegin</label>
            <form:input path="taskDTO.dateBegin" type="date" class="form-control" id="inputDateBegin" name="dateBegin"/>
        </div>
        <div class="form-group col-md-2">
            <label for="inputDateEnd">DateEnd</label>
            <form:input path="taskDTO.dateEnd" type="date" class="form-control" id="inputDateEnd" name="dateEnd"/>
        </div>
        <div class="form-group col-md-2">
            <label for="projectName">Project</label>
            <select class="form-control" id="projectName" name="projectId">
                <c:forEach var="projectDTO" items="${projectDTOList}">
                    <c:if test="${taskDTO.projectId == projectDTO.id}">
                        <option selected value="${projectDTO.id}">${projectDTO.name}</option>
                    </c:if>
                    <c:if test="${taskDTO.projectId != projectDTO.id}">
                        <option value="${projectDTO.id}">${projectDTO.name}</option>
                    </c:if>
                </c:forEach>
            </select>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="inputDescription">Description</label>
            <input type="text" class="form-control" id="inputDescription" name="description" value="${taskDTO.description}">
        </div>
        <div class="form-group col-md-4">
            <label for="inputStatus">Status</label>
            <select class="form-control" id="inputStatus" name="readinessStatus">
                <c:forEach var="status" items="${statusList}">
                    <c:if test="${taskDTO.readinessStatus == status}">
                        <option selected value="${status}">${status.displayName}</option>
                    </c:if>
                    <c:if test="${taskDTO.readinessStatus != status}">
                        <option value="${status}">${status.displayName}</option>
                    </c:if>
                </c:forEach>
            </select>
        </div>
    </div>
    <input class="btn btn-dark" type="submit" value="Done">
</form>
<table>
    <tr>
        <td>
            <form action="/tasks" method="get" style="padding-left: 50px">
                <input class="btn btn-dark" type="submit" value="Abort">
            </form>
        </td>
    </tr>
</table>
</body>
</html>